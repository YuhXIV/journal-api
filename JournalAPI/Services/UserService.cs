﻿using JournalAPI.Helpers;
using JournalAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JournalAPI.Services
{
    public class UserService : IUserService
    {
        private JournalContext _journalContext;

        public UserService(JournalContext journalContext)
        {
            _journalContext = journalContext;
        }

        public User Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return null;
            }

            var user = _journalContext.Users.SingleOrDefault(user => user.Username == username);

            //return null if user does not exist
            if (user == null)
            {
                return null;
            }

            //Verify if the password is correct
            if(!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }

            return user;
        }

        public IEnumerable<User> GetAll()
        {
            return _journalContext.Users;
        }

        public User GetById(long id)
        {
            return _journalContext.Users.Find(id);
        }

        public User Create(User user, string password)
        {
            //Checking params
            if(string.IsNullOrWhiteSpace(password))
            {
                throw new AppException("Password is required");
            }

            if (_journalContext.Users.Any(x => x.Username == user.Username))
            {
                throw new AppException("Username \"" + user.Username + "\" is already taken");
            }

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            _journalContext.Users.Add(user);
            _journalContext.SaveChanges();

            return user;
        }

        public void Update(User userInfo, string password = null)
        {
            var user = _journalContext.Users.Find(userInfo.Id);

            if(user == null)
            {
                throw new AppException("User not found");
            }

            if (userInfo.Username != user.Username)
            {
                // username has changed so check if the new username is already taken
                if (_journalContext.Users.Any(x => x.Username == userInfo.Username))
                {
                    throw new AppException("Username " + userInfo.Username + " is already taken");
                }
            }

            // update user properties
            user.Username = userInfo.Username;

            // update password if it was entered
            if (!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            _journalContext.Users.Update(user);
            _journalContext.SaveChanges();
        }

        public void Delete(long id)
        {
            var user = _journalContext.Users.Find(id);

            if(user != null)
            {
                _journalContext.Users.Remove(user);
                _journalContext.SaveChanges();
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            if (string.IsNullOrWhiteSpace(password)) 
            {
                throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            }

            if (storedHash.Length != 64)
            {
                throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            }

            if (storedSalt.Length != 128)
            {
                throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");
            }

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            }

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
    }
}

﻿using System.Collections.Generic;
using JournalAPI.Models;

namespace JournalAPI.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
        User GetById(long id);
        User Create(User userInfo, string password);
        void Update(User user, string password = null);
        void Delete(long id);
    }
}

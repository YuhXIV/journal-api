﻿using Microsoft.EntityFrameworkCore;

namespace JournalAPI.Models
{
    public class JournalContext : DbContext
    {
        public JournalContext(DbContextOptions<JournalContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<JournalEntry> JournalEntries { get; set; }
    }
}

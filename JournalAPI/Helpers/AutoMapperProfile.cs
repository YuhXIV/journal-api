﻿using JournalAPI.Dtos;
using JournalAPI.Models;
using AutoMapper;

namespace JournalAPI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}
